# GitLab CI template for Postman

This project implements a GitLab CI/CD template to run your automated (API) tests with [Postman](https://www.postman.com/automated-testing).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: $CI_SERVER_FQDN/to-be-continuous/postman/gitlab-ci-postman@3.7.0
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      collections: "e2e/*collection.json"
      review-enabled: "true"
```

### Use as a CI/CD template (legacy)

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/postman'
    ref: '3.7.0'
    file: '/templates/gitlab-ci-postman.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  POSTMAN_COLLECTIONS: "e2e/*collection.json"
  REVIEW_ENABLED: "true"
```

## `postman` job

This job starts [Postman automated tests](https://www.postman.com/automated-testing).

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `image` / `POSTMAN_IMAGE` | The Docker image used to run Postman CLI. | `registry.hub.docker.com/postman/newman:latest` <br/>[![Trivy Badge](https://to-be-continuous.gitlab.io/doc/secu/trivy-badge-POSTMAN_IMAGE.svg)](https://to-be-continuous.gitlab.io/doc/secu/trivy-POSTMAN_IMAGE) |
| `collections` / `POSTMAN_COLLECTIONS` | The matcher to select Postman collection file(s) to run. | `postman/*collection.json` |
| `extra-args` / `POSTMAN_EXTRA_ARGS` | Newman extra [run options](https://github.com/postmanlabs/newman#command-line-options) (to use global variables, an environment or a data source for e.g.) | _none_ |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Postman tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/postman-*.xunit.xml` | [JUnit](https://github.com/postmanlabs/newman#junitxml-reporter) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### Postman `{{base_url}}` auto evaluation

By default, the Postman template auto-evaluates a [{{base_url}} variable](https://learning.postman.com/docs/postman/variables-and-environments/variables/)
(i.e. the variable pointing at server under test) by looking either for a `$environment_url` variable or for an
`environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Postman test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Postman tests.

### Hook scripts

The Postman template supports _optional_ **hook scripts** from your project, located in the root directory to perform additional project-specific logic:

* `pre-postman.sh` is executed **before** running Postman,
* `post-postman.sh` is executed **after** running Postman (whichever the tests status).
